/*:
 * @plugindesc Plugin for kanji illustrator
 * @version 1.0
 
@param Key K
@desc The common event to play when K is pressed.
Use 0 to have that not trigger a common event.
@default 0
 
 
 */
 
var parameters = PluginManager.parameters('KeyPressK');
var keyEvent = Number(parameters['Key K'] || 0);
 
(function ($) {
   
    // Input.isPressed = function(keyName) {
    // if (this._isEscapeCompatible(keyName) && this.isPressed('escape')) {
        // return true;
    // } else {
        // return !!this._currentState[keyName];
    // }
// };
   
    var XGD_PluginEvent = Input._onKeyUp;
    Input._onKeyUp = function(event) {
    XGD_PluginEvent.call(this, event);
    if (event.keyCode === 75 && keyEvent !== 0) //K is pressed
        $gameTemp.reserveCommonEvent(keyEvent);
};
})();