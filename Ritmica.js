/*
 * 
 * @plugindesc GC's Rhythm Battle System: An M3-inspired
 * Rhythm combo system.
 * @author GeigerCounter ( C.C. Warren )
 * 
 * @param tempo
 * @desc Default fallback tempo when not defined in Troop specs
 * @default 140
 * 
 * @param waltz
 * @desc Waltz time? 0=False, 1=True
 * @default 0
 * 
 * @param beat
 * @desc The beat pattern, expressed as a binary 8-bit int
 * @default 10101010
 * 
 * @help
 * This still needs to be written, but the basic gist
 * Is that for every encounter, there'll be a call to the
 * Rhythm Battle System supplying three parameters
*/

var GCRhythms = GCRhythms || {};
GCRhythms.parameters = PluginManager.parameters('GCRhythm_Battle');

//======================================================================
// * 
//======================================================================
