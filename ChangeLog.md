# Cognitive Dissonance : Remade


## 2020-07-13

Moved repo from private server to official GitLab domain. Finally.

## 2018-04-28

Added MV Master Demo into Misc.

Added Weather EX plugin.

Added Greyface to debug room.

Gave Greyface control over the weather.

Decided that this was a horrible idea, but went along with it anyway.


## 2018-04-20

Copied fonts from /Misc/Fonts to the game's font folder.

Added some plugins.

Tweaked the one room in the game, currently in use as a debug room, to look a bit better.

Obligatorily celebrated 4/20.


## 2018-04-15

Added Misc folder.

Added M3 soundfont files.


## 2018-04-14

Added spritesheet for Niiue.

Added item names and basic functionality.

Added some status conditions.

Added ChangeLog.md.


## 2018-04-13

Initial commit.

Added RPGMV game files.

Added spritesheet for Giegue.